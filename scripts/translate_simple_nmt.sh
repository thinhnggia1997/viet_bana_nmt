#!/bin/bash

MODEL_FILE=./runs/model.pth
GPU_DEVICE=0
LANGUAGE=enko

PYTHONPATH=./ python tools/translate.py --model_fn MODEL_FILE --gpu_id GPU_DEVICE --lang LANGUAGE < test.txt > test.result.txt

